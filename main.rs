use std::io::stdin;

fn is_isbn10() -> bool{
    // crear y comentar esta función
    


    return true
}


fn is_isbn_format_valid(c: &str) -> bool {

    // Esta función comprueba si el número fue ingresado en el formato correcto,
    // y si el último dígito es una x lo detecte.
    if c.chars().next().unwrap().is_numeric() {
        return true;
    } else if c == "X" || c == "x"{
        return true;
    }
    return false
}

fn main(){

    let mut isbn: String = String::new();
    let mut clean_isbn: String = String::new();
    stdin().read_line(&mut isbn).unwrap();
   
    // Cada dígito ingresado es convertido en string en el caso de que el 
    // formato sea válido.Además con la función trim se eliminan los espacios
    // en blanco que tenga la variable isbn.
    for c in isbn.to_string().trim().chars(){
        if is_isbn_format_valid(&c.to_string()){
            clean_isbn = clean_isbn + &c.to_string(); 
        }
    }

    println!("{}", clean_isbn);
    // Si el número ingresado da como resultado 0 se determina que será válido,
    // de lo contrario se mostrará en pantalla el mensaje de que no lo es.
    if is_isbn10(clean_isbn){
        println!("{} es un ISBN10 valido", clean_isbn);
    } else {
        println!("No lo es");
    }

}
